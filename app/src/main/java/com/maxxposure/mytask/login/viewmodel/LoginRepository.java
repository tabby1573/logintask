package com.maxxposure.mytask.login.viewmodel;

import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;

import com.maxxposure.mytask.ScrHome;
import com.maxxposure.mytask.communication.ApiInterface;
import com.maxxposure.mytask.communication.Constants;
import com.maxxposure.mytask.communication.RetrofitBase;
import com.maxxposure.mytask.login.ScrLogin;
import com.maxxposure.mytask.sharedpref.UserData;
import com.maxxposure.mytask.utils.CustomDialog;
import com.maxxposure.mytask.utils.CustomIntent;
import com.maxxposure.mytask.utils.CustomToast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginRepository {

    private String TAG = "LoginRepository";
    ApiInterface apiInterface;
    private ScrLogin context;


    public LoginRepository(ScrLogin context) {
        this.context = context;
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
    }


    public void callLoginAPI(final String username, String password) {

        if (TextUtils.isEmpty(username)) {
            CustomToast.showToast(context, "Please enter email");
            return;
        }
        if (TextUtils.isEmpty(password)) {
            CustomToast.showToast(context, "Please enter password");
            return;
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("username", username);
        params.put("password", password);
        call_login_api(params);
    }

    public void call_login_api(HashMap<String, String> params) {
        CustomDialog.showDialog(context, Constants.PROGRESS_MSG);
        Call<String> call = apiInterface.login(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                CustomDialog.closeDialog(context);
                Log.d(TAG, "onResponse: call_login_api : " + response.body());
                Log.d(TAG, "onResponse: call_login_api url : " + call.request().url().toString());
                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject object = new JSONObject(response.body());
                    boolean status = object.getBoolean("status");
                    String message = object.getString("message");
                    if (status) {
                        JSONObject data = object.getJSONObject("data");
                        String Token = data.getString("Token");
                        UserData.getInstance().setTOKEN(Token);
                        CustomIntent.startActivity(context, ScrHome.class,false);
                    } else {
                        CustomToast.showToast(context, message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(TAG, "onResponse: JSONException " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CustomDialog.closeDialog(context);
            }
        });
    }


}
