package com.maxxposure.mytask.login.viewmodel;

import com.google.gson.annotations.SerializedName;

public class Home {

    @SerializedName("name")
    private String name;
    @SerializedName("newCount")
    private String newCount;
    @SerializedName("dispatchedCount")
    private String dispatchedCount;
    @SerializedName("deliveredCount")
    private String deliveredCount;
    @SerializedName("cashInhand")
    private String cashInhand;

    public Home(String name, String newCount, String dispatchedCount, String deliveredCount, String cashInhand) {
        this.name = name;
        this.newCount = newCount;
        this.dispatchedCount = dispatchedCount;
        this.deliveredCount = deliveredCount;
        this.cashInhand = cashInhand;
    }

    public Home() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNewCount() {
        return newCount;
    }

    public void setNewCount(String newCount) {
        this.newCount = newCount;
    }

    public String getDispatchedCount() {
        return dispatchedCount;
    }

    public void setDispatchedCount(String dispatchedCount) {
        this.dispatchedCount = dispatchedCount;
    }

    public String getDeliveredCount() {
        return deliveredCount;
    }

    public void setDeliveredCount(String deliveredCount) {
        this.deliveredCount = deliveredCount;
    }

    public String getCashInhand() {
        return cashInhand;
    }

    public void setCashInhand(String cashInhand) {
        this.cashInhand = cashInhand;
    }
}
