package com.maxxposure.mytask.login;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.maxxposure.mytask.R;
import com.maxxposure.mytask.databinding.ActivityScrLoginBinding;
import com.maxxposure.mytask.login.viewmodel.LoginRepository;
import com.maxxposure.mytask.login.viewmodel.LoginUser;
import com.maxxposure.mytask.login.viewmodel.LoginViewModel;

import java.util.Objects;

public class ScrLogin extends AppCompatActivity {

    private LoginViewModel viewModel;
    private ActivityScrLoginBinding databinding;
    private LoginRepository loginRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scr_login);
        //init repository
        loginRepository = new LoginRepository(this);
        //view model
        viewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        //databinding
        databinding = DataBindingUtil.setContentView(this, R.layout.activity_scr_login);
        // databinding.setLifecycleOwner(this);

        //click handler
        databinding.setLoginviewmodel(viewModel);
        viewModel.getUser().observe(this, new Observer<LoginUser>() {
            @Override
            public void onChanged(@Nullable LoginUser loginUser) {

                if (TextUtils.isEmpty(Objects.requireNonNull(loginUser).getStrEmailAddress())) {
                    databinding.etUsername.setError("Enter email");
                    databinding.etUsername.requestFocus();
                }else if(!Patterns.EMAIL_ADDRESS.matcher(Objects.requireNonNull(loginUser).getStrEmailAddress()).matches()){
                    databinding.etUsername.setError("Enter valid email");
                    databinding.etUsername.requestFocus();
                }
                else if (TextUtils.isEmpty(Objects.requireNonNull(loginUser).getStrPassword())) {
                    databinding.etPassword.setError("Enter a password");
                    databinding.etPassword.requestFocus();
                } else {
                    databinding.etUsername.setText(loginUser.getStrEmailAddress());
                    databinding.etPassword.setText(loginUser.getStrPassword());
                    loginRepository.callLoginAPI(loginUser.getStrEmailAddress(), loginUser.getStrPassword());

                }
            }
        });
    }


}