package com.maxxposure.mytask;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;

import androidx.multidex.MultiDexApplication;


public class TaskApplication extends MultiDexApplication {

    public static Context appContext;

    public static final String CHANNEL_ID = "trackinglocation";


    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;
    }

    public static synchronized TaskApplication getInstance() {
        return (TaskApplication) appContext;
    }




}