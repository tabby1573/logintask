package com.maxxposure.mytask.sharedpref;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.maxxposure.mytask.TaskApplication;



public class UserData {

    private static UserData _instance = null;
    private static SharedPreferences _sharedPreferences = null;
    private static Editor _sharedPrefEditor = null;
    private final String SHARED_PREFERENCE_NAME = "user_data";


    private final String TOKEN = "TOKEN";



    private UserData() {

    }

    public static UserData getInstance() {
        if (_instance == null) {
            _instance = new UserData();
            _instance._initSharedPreferences();
        }
        return _instance;
    }

    /**
     * This method is used to initialized {@link SharedPreferences} and
     * {@link Editor}
     */
    public void _initSharedPreferences() {
        _sharedPreferences = _getSharedPref();
        _sharedPrefEditor = _getSharedPrefEditor();
    }

    /**
     * Method to get the SharedPreferences.
     *
     * @return the {@link SharedPreferences} object.
     */
    private SharedPreferences _getSharedPref() {
        if (_sharedPreferences == null) {
            _sharedPreferences = TaskApplication.appContext.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        }
        return _sharedPreferences;
    }

    /**
     * Method to get the {@link Editor} for writing values to {@link SharedPreferences}.
     *
     * @return the {@link Editor} object.
     */
    private Editor _getSharedPrefEditor() {
        if (_sharedPrefEditor == null) {
            _sharedPrefEditor = _getSharedPref().edit();
        }
        return _sharedPrefEditor;
    }


    public void setTOKEN(String token) {
        _getSharedPrefEditor().putString(TOKEN, token).commit();
    }

    public String getTOKEN() {
        return _getSharedPref().getString(TOKEN, null);
    }



    public void clearData() {
        _getSharedPrefEditor().clear();
        _getSharedPrefEditor().commit();
    }


}
