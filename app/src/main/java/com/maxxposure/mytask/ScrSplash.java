package com.maxxposure.mytask;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;

import com.maxxposure.mytask.login.ScrLogin;
import com.maxxposure.mytask.utils.CustomIntent;

public class ScrSplash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scr_splash);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startNextScreen();
    }

    private void startNextScreen() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                CustomIntent.startActivity(ScrSplash.this, ScrLogin.class, true);
            }
        }, 3000);
    }
}