package com.maxxposure.mytask.communication;

public class Constants {

    public static final String PROGRESS_MSG = "Please Wait...";
    public static final String WENT_WRONG = "Something went wrong please try again later";
    public static final String FETCH_ERROR = "Data fetch error";
    public static final String SERVER_ERROR = "Server error";



}
