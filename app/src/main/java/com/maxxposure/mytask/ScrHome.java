package com.maxxposure.mytask;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.maxxposure.mytask.databinding.ActivityScrHomeBinding;
import com.maxxposure.mytask.sharedpref.UserData;

public class ScrHome extends AppCompatActivity {

    private ActivityScrHomeBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_scr_home);
        binding.tvToken.setText("User Token : " + UserData.getInstance().getTOKEN());
    }


}